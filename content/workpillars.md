+++
title = "Work Pillars"
description = "Our work pillars"
+++

## 1. FUNCTIONAL FEED & INGREDIENTS

Tailored valorization and upgrade of agro-food by-products to functional feeds and ingredients.

Selection of innovative microbial biomass with multi-purpose functions.

Examples include: 
* bioactive plant extracts
* microbial protein
* microbial metabolites​
* enzymes ​
* various fermentates (ensilage, SSF)

## 2. INNOVATION TECHNOLOGY PLATFORM

Involves multi-level and multi-actor  critical process technologies and bio-technologies​ e.g., extractions, fermentations, enzymatic hydrolysis, fractionations downstream processing, chromatographic, biochemical and molecular analytics.
Examples include: ​
* Biomarker selection for advanced homeostasis monitoring ​
* Stress Challenge models and resilience  ​
* Functional Indices linking performance with biometrics  performance  ​
* Nutrigenomics ​
* Diagnostic tools ​
* Typical analytical methods (Weende, ORAC, TBARS, antioxidant enzymes, carbonyls, texture analyses, egg analyses, PCR etc)  

## 3. SERVICES

* Diet Formulation with functional feeds, specialized products and sustainable new ingredients​
* Comprehensive in-depth documentation of livestock products’ quality via multiple assay biomarkers   ​
* Monitoring nutritional applications on field for documenting tailored dietary applications. ​
* Consulting services for the sustainable development of animal production.


---

> In case you haven't found the answer for your question please feel free to contact us, our customer support will be happy to help you.
