+++
title = "Selected Publications"
date = "2024-07-21T13:50:46+02:00"
tags = ["news","company"]
categories = ["central"]
description = "Establishment of Feedit as a spin off company of the Agricultural University of Athens (AUA) ​."
banner = "https://schema-united.gitlab.io/www/feedit/img/science.jpg"
authors = ["feedit"]
+++



- Mountzouris K.C et al. (2009) Phytogenic compounds in broiler nutrition. In: Phytogenics in animal nutrition, pp 97-110, ed Steiner T., Nottingham University Press, Nottingham.
- Mountzouris KC et al., (2009) Effects of Lactobacillus acidophilus on gut microflora metabolic biomarkers in fed and fasted rats. Clinical Nutrition 28:318-324.
- Mountzouris KC et al., (2009) Effects of a multi-species probiotic on biomarkers of competitive exclusion efficacy in broilers challenged with Salmonella enteritidis. British Poultry Science 50: 467-478.
- Mountzouris KC (2014) Probiotics as alternatives to antimicrobial growth promoters in broiler nutrition: modes of action and effects on performance In: Probiotics in Poultry Production, ed Abdelrahman W.H.A., Nottingham University Press, Nottingham.
- Mountzouris KC et al., (2015) Evaluation of yeast dietary supplementation in broilers challenged or not with Salmonella on growth performance, cecal microbiota composition and Salmonella in ceca, cloacae and carcass skin. Poultry Science 94: 2445-2455.
- Paraskeuas, V et al. (2021) Effects of Deoxynivalenol and Fumonisins on Broiler Gut Cytoprotective Capacity. Toxins, 13, 729. https://doi.org/ 10.3390/toxins13100729
- Mountzouris  KC (2022). Prebiotics: Types. In: McSweeney, P.L.H., McNamara, J.P. (Eds.), Encyclopedia of Dairy Sciences, vol. 4. Elsevier, Academic Press, pp. 352–358. 
- Mountzouris KC et al. 2022  Adaptive Poultry Gut Capacity to Resist Oxidative Stress. In: Kogut, M.H., Zhang, G. (eds) Gut Microbiota, Immunity, and Health in Production Animals. The Microbiomes of Humans, Animals, Plants, and the Environment, 4, 243-262, 2022
- Palamidi I et al. (2022) Dietary and phytogenic inclusion effects on the broiler chicken cecal ecosystem. Frontiers in Animal Science. 3: 1094314
- Paraskeuas VV et al (2023) Dietary Inclusion Level Effects of Yoghurt Acid Whey Powder on Performance, Digestibility of Nutrients and Meat Quality of Broilers. Animals. 13(19), 3096. 
- Palamidi I et al. (2023) Effect of Yogurt Acid Whey on the Quality of Maize Silage. Fermentation. 9(12), 994. 
- Brouklogiannis IP et al. (2023) Dietary phytogenic inclusion level affects production performance and expression of ovarian cytoprotective genes in laying hens. Poult Sci. 102(4), 102508. 
- Mountzouris KC & Brouklogiannis IP (2024) Phytogenics as natural gut health management tools for sustainable poultry pr