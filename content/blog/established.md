+++
title = "FeedIt has been established!"
date = "2024-06-24T13:50:46+02:00"
tags = ["news","company"]
categories = ["central"]
description = "Establishment of Feedit as a spin off company of the Agricultural University of Athens (AUA) ​."
banner = "https://schema-united.gitlab.io/www/feedit/img/rooster_New-2.fw.png"
authors = ["feedit"]
+++

## Let's get started!

Establishment of Feedit as a spin off company of the Agricultural University of Athens (AUA) .

In March 2024 the AUA governing council approved the set up of Feed Innovations and Technologies P.C. as an AUA spin off company. ​

Subsequently FeedIT was founded in April 2024 by Prof. K.C. Mountzouris and his team members Dr. V. Paraskeuas, Dr. I. Palamidi and Mr. I. Brouklogiannis.