+++
title = "Innovation Technology Platform"
id = "simplewithimage"
sideimage="https://schema-united.gitlab.io/www/feedit/img/diagnostics.png"
+++


Involves multi-level and multi-actor  critical process technologies and bio-technologies e.g., extractions, fermentations, enzymatic hydrolysis, fractionations downstream processing, chromatographic, biochemical and molecular analytics  

Examples include: 
- Biomarker selection for advanced homeostasis monitoring 
- Stress Challenge models and resilience  
- Functional Indices linking performance with biometrics  performance  
Nutrigenomics 
- Diagnostic tools 
- Typical analytical methods (Weende, ORAC, TBARS, antioxidant enzymes, carbonyls, texture analyses, egg analyses, PCR etc)  