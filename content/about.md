+++
title = "About"
id="team"
allowcontent = "yes"
description ="Feed Innovations & Technologies P.C. (FeedIT) is a spin off company of the Agricultural University of Athens that was founded in April 2024."
+++
<br/>
<br/>
► Feed Innovations & Technologies P.C. (FeedIT) is a spin off company of the Agricultural University of Athens that was founded in April 2024. ​

► The aim of FeedIT  to exploit the long term know how of Prof. KC Mountzouris and his team to design innovative nutritional products and technological tools and support services that will provide natural, economic, efficient and sustainable solutions in the Animal Production industry and the agri-food sector.​
​
### FeedIT team members

- Prof. K.C. Mountzouris <br/>
Managing Director, responsible for FeedIT nutritional products, communications, finance and growth development 
<br/>
<br/>
- Dr V.V. Parakeuas <br/>
Assistant Professor, responsible for experimentation, research programs management and administration 
<br/>
<br/>
- Dr I.N. Palamidi<br/>
Postdoctoral researcher, responsible for research and development activities, focus on microbials 
<br/>
<br/>
- Mr I.P.  Brouklogiannis</br>
PhD student, responsible for research and development activities, focus on analytical technological tools
<br/>
<br/>
- The expertise of the group of shareholders will be augmented on a subcontracting basis by expert scientists (e.g. chemical engineer, chemist, food technologist, biotechnologist, environmentalist, etc.)
<br/>
<br/>
- FeedIT has a dedicated outsourced accounting and legal advice office.
<br/>
<br/>
<br/>
<br/>