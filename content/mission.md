+++
title = "Mission"
description = "This is the mission of FeedIt"
+++
<br/>
<br/>
<br/>
<br/>

**“Innovation in the design and production of functional feeds & ingredients, technological tools and specialized services that generate a distinct quality and sustainability signature for animal production and the agri-food sector”**
<br/>
<br/>
<br/>
<br/>
We aim to create value by addressing contemporary market needs for:

* Innovative nutritional and technological products that can assist in effectively steering Animal Production towards the “golden ratio” of efficiency, health, resilience and product quality leading to optimal sustainability 

* State of the art substantiation of how critical dietary components function in the context of the overall diet and environment 

* Scientifically proven dietary solutions and services creating  positive imprints on One Health, Circular economy and Green deal roadmaps.
<br/>
<br/>
<br/>
<br/>