+++
title = "Contact"
+++

Head office: <br/>
Work Up Hub <br/>
Matsa 10 Str, Kifissia,  <br/>
145 64, Greece <br/>
Tel: +30 210 2431847 <br/>
email1: info@feedit.gr <br/>
e-mail2: kmountzouris@aua.gr <br/>
Google maps:  https://maps.app.goo.gl/TTKTtPb7uNrcpkmr5
 <br/>
  <br/>
   <br/>

You can also find us at the: <br/>
Agricultural University of Athens:  <br/>
Nutritional Physiology and Feeding Lab  <br/>
Iera Odos 86, Athens 11855  <br/>
Tel: +30 2102431847   <br/>
Tel: +30 210 5294422 <br/>
Google maps: https://maps.app.goo.gl/pX6nkZxviu9XqS2fA
 <br/>
  <br/>
   <br/>
