+++
title = "Functional Feed & Ingredients"
id = "simplewithimage"
sideimage="https://schema-united.gitlab.io/www/feedit/img/ingredients.png"
+++
<br/>
<br/>
<br/>
<br/>
Tailored valorization and upgrade of agro-food by-products to functional feeds and ingredients e.g.,  functional feed ingredients and specialty mixtures of high value olive fruit bioactive compounds produced via proprietary valorization technologies from olive process waste
 
Functional microbial proteins and metabolites with multi-purpose functions produced via fermentation technologies on selected substrate matrixes 
  
Plans under development: 
- bioactive plant extracts
- microbial enzymes 
- various fermentates (ensilage, SSF) 
<br/>
<br/>
<br/>
<br/>