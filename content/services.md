+++
title = "Services"
id = "simplewithimage"
sideimage="https://schema-united.gitlab.io/www/feedit/img/handshake_emoji.png"
+++


**Tailored services for measuring and validating nutritional solutions for animal production efficiency, resilience, health and product quality.**

- Diet Formulation with functional feeds, specialized products and sustainable new ingredients
- Advanced monitoring of critical homeostasis elements responsive to nutrition and the environment (e.g. heat)
- Comprehensive in-depth documentation of livestock products’ quality via multiple assay biomarkers   
- Monitoring nutritional applications on field for documenting tailored dietary applications. 
- Consulting services for the sustainable development of animal production.
