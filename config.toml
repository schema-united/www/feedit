baseURL = "https://schema-united.gitlab.io/www/feedit/"
languageCode = 'en-us'
title = 'FeedIt'

# Site language. Available translations in the theme's `/i18n` directory.
defaultContentLanguage = "en"

# number of words of summarized post content (default 70)
summaryLength = 70

# Define the number of posts per page
paginate = 10

# not pluralize title pages by default
pluralizelisttitles = false

[menu]


# Main menu
[[menu.main]]
    name       = "Home"
    identifier = "menu.home"
    url        = "https://feedit.gr/"
    weight     = 1

[[menu.main]]
    name       = "About Us"
    identifier = "menu.about"
    url        = "https://feedit.gr/about/"
    weight     = 2

[[menu.main]]
    name       = "Mission"
    url        = "https://feedit.gr/mission/"
    weight     = 3
    identifier = "menu.mission"

[[menu.main]]
    name       = "Work Pillars"
    identifier = "menu.workpillars"
    url        = "https://feedit.gr/workpillars/"
    weight     = 5

[[menu.main]]
    name       = "Functional Feed & Ingredients"
    identifier = "menu.feed"
    url        = "https://feedit.gr/feed/"
    weight     = 1
    parent     = "menu.workpillars"

[[menu.main]]
    name       = "Innovation Technology Platform"
    identifier = "menu.innovation"
    url        = "https://feedit.gr/innovation/"
    weight     = 2
    parent     = "menu.workpillars"

[[menu.main]]
    name       = "Services"
    identifier = "menu.services"
    url        = "https://feedit.gr/services/"
    weight     = 2
    parent     = "menu.workpillars"

[[menu.main]]
    name       = "News"
    identifier = "menu.blog"
    url        = "https://feedit.gr/blog/"
    weight     = 6

[[menu.main]]
    identifier = "contact"
    name       = "Contact"
    url        = "https://feedit.gr/contact/"
    weight     = 7


# Top bar social links menu

[[menu.topbar]]
    weight = 1
    name = "Phone"
    url = "tel: +30 214 40 68 460 "
    pre = "<i class='fas fa-2x fa-phone'></i>"

[[menu.topbar]]
    weight = 3
    name = "Facebook"
    url = "http://facebook.com"
    pre = "<i class='fab fa-2x fa-facebook'></i>"

[[menu.topbar]]
    weight = 4
    name = "Twitter"
    url = "http://twitter.com"
    pre = "<i class='fab fa-2x fa-twitter'></i>"

[[menu.topbar]]
    weight = 5
    name = "Email"
    url = "mailto:info@feedit.gr"
    pre = "<i class='fas fa-2x fa-envelope'></i>"

[params]
    alternate_url = "https://feedit.gr/"
    viewMorePostLink = "/blog/"
    author = "FeedIt"
    defaultKeywords = ["feedit"]
    mainSections = ["blog"]
    defaultDescription = "Feedit, Nutrition Worktunes"

    # Social media
    facebook_site = "" # the Facebook handle of your site ('https://www.facebook.com/HANDLE')
    twitter_site = "GoHugoIO" # the Twitter handle of your site (without the '@')
    default_sharing_image = "img/sharing-default.png"

    # Google Maps widget: If `googleMapsApiKey` is not set, no key will be passed to Google (which likely results in a broken map widget).
    enableGoogleMaps = false
    googleMapsApiKey = "AIzaSyAv7Sza8NSp9_l_g8G2vlo0H4ydEPn_2jY"
    latitude = "38.08100897302105"
    longitude = "23.79029933459401"

    # Style options: default (light-blue), blue, green, marsala, pink, red, turquoise, violet
    style = "turquoise"

    # Since this template is static, the contact form uses www.formspree.io as a
    # proxy. The form makes a POST request to their servers to send the actual
    # email. Visitors can send up to a 50 emails each month for free.
    #
    # What you need to do for the setup?
    #
    # - register your account to https://formspree.io/register
    # - login and create new form
    # - set your form's endpoint url under 'formspree_action' below
    # - upload the generated site to your server
    # - test a dummy email yourself
    # - you're done. Happy mailing!
    #
    # Enable the contact form by entering your Formspree.io endpoint url
    formspree_action = "https://formspree.io/sample/of/endpoint"
    contact_form_ajax = false

    # Formspree form supports Google reCAPTCHA Key (type v2).
    # If you use this feature, you should enable reCAPTCHA feature in the Formspree dashboard.
    #
    # By default, Formspree use a redirect page for recaptcha widget.
    # If you use a recaptcha widget in your contact page, you should do next steps.
    # (if you don't want, skip these steps)
    #
    #   1. register your site on Google recaptcha admin page: https://www.google.com/recaptcha/admin
    #   2. select reCAPTCHA v2 and checkbox widget type.
    #   3. remember site key and secret key.
    #   4. enter secret key into "Custom reCAPTCHA Key" field in your Formspree form setting page.
    #   5. change `enableRecaptchaInContactForm` is to true
    #   6. enter site key into `googleRecaptchaKey` to enable a recaptcha widget in your page.
    #
    enableRecaptchaInContactForm = false
    googleRecaptchaKey = "site_key_for_google_recaptcha"

    about_us = "<p>Our Mission: Innovation in the design and production of functional feeds & ingredients, technological tools and specialized services that generate a distinct quality and sustainability signature for animal production and the agri-food sector</p>"
    copyright = "Copyright (c) 2024,FeedIt; all rights reserved."

    # Format dates with Go's time formatting
    date_format = "January 2, 2006"

    dropdown_mouse_over = false

    disabled_logo = false
    logo_text = "Feedit"

    logo = "https://schema-united.gitlab.io/www/feedit/img/logo.fw.png"
    logo_small = "https://schema-united.gitlab.io/www/feedit/img/logo.fw.png"
    contact_url = "https://feedit.gr/contact/"
    main_address = """<p class="text-uppercase"><strong>Head Office</strong>
        <br>Work Up Hub
        <br>Matsa 10 Str, Kifissia, 
        <br>145 64, 
        <br>Tel: +30 2102431847
        <br>
      </p>
    """
    secondary_address = """<p class="text-uppercase">
        <strong>Agricultural University of Athens</strong>
        <br>Nutritional Physiology and Feeding Lab
        <br>Iera Odos 86, Athens 11855
        <br>145 64, 
        <br>Iera Odos 86, Athens 11855
        <br>e-mail: kmountzouris@aua.gr
        <br>
        <strong>Greece</strong>
      </p>
      """
    address = """<p class="text-uppercase"><strong>Head Office</strong>
        <br>Work Up Hub
        <br>Matsa 10 Str, Kifissia, 
        <br>
      </p>
      """
    maps_image = "/img/matsa.png"
    maps_link = "https://maps.app.goo.gl/LRE16kH83mCba2Tt6"

[permalinks]
    blog = "/blog/:year/:month/:day/:filename/"

# Enable or disable top bar with social icons
[params.topbar]
    enable = true
    text = """<p class="hidden-sm hidden-xs">Contact us on +30 2102431847 or info@feedit.gr</p>
      <p class="hidden-md hidden-lg"><a href="tel:+30 2102431847" data-animate-hover="pulse"><i class="fas fa-phone"></i></a>
      <a href="mailto:info@feedit.gr" data-animate-hover="pulse"><i class="fas fa-envelope"></i></a>
      </p>
      """

# Enable and disable widgets for the right sidebar
[params.widgets]
    categories = true
    tags = true
    search = true

[params.carouselCustomers]
    items = 6
    auto_play = false
    slide_speed = 2000
    pagination_speed = 1000

[params.carouselTestimonials]
    items = 4
    auto_play = false
    slide_speed = 2000
    pagination_speed = 1000

[params.carouselHomepage]
    # All carousel items are defined in their own files. You can find example items
    # at 'exampleSite/data/carousel'.
    # For more information take a look at the README.
    enable = false
    auto_play = true
    slide_speed = 2000
    pagination_speed = 1000

[params.headerHomepage]
    # All carousel items are defined in their own files. You can find example items
    # at 'exampleSite/data/carousel'.
    # For more information take a look at the README.
    enable = true


[params.features]
    enable = true
    cols = 1 # Default: 3, Available values 2,3,4,6
    # All features are defined in their own files. You can find example items
    # at 'exampleSite/data/features'.
    # For more information take a look at the README.

[params.see_more]
    enable = true
    icon = "far fa-file-alt"
    title = ""
    subtitle = ""
    link_url = "https://feedit.gr/worktunes/"
    link_text = "Check more info"

[params.clients]
    enable = true
    # All clients are defined in their own files. You can find example items
    # at 'exampleSite/data/clients'.
    # For more information take a look at the README.
    title = "Our Clients"
    subtitle = ""

[params.recent_posts]
    enable = true
    title = "Our news"
    subtitle = "Read our latest news and articles."
    hide_summary = false

[params.footer.recent_posts]
    enable = true

[taxonomies]
  category = "categories"
  tag = "tags"
  author = "authors"


[markup]
  [markup.goldmark]
    [markup.goldmark.renderer]
      unsafe = true

[outputs]
    home = ["HTML", "JSON"]  # or "JSON" alone if no HTML needed
[outputFormats]
    [outputFormats.JSON]
        mediaType = "application/json"
        baseName = "searchindex"
        isPlainText = true