document.addEventListener('DOMContentLoaded', function() {
    const gear = document.querySelector('.gear');
    const center = document.getElementById('hover-rotate');
    function startRotation() {
        // Clear any existing timeout to avoid conflicts
        if (gear.rotationTimeout) {
            clearTimeout(gear.rotationTimeout);
        }

        // Add the rotate class
        gear.classList.add('rotate');

        // Remove the rotate class after 5 seconds
        gear.rotationTimeout = setTimeout(() => {
            gear.classList.remove('rotate');
        }, 3000);
    }
    if (center){
    // Rotate on hover
        center.addEventListener('mouseover', function(){startRotation()});
        center.addEventListener('mouseout', function(){
            gear.classList.remove('rotate');
        });
    }
    if (gear){
    // Rotate on page load
    startRotation();    
    }
});

