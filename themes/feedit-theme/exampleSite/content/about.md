+++
title = "About Us"
id = "team"
description ="Feed Innovations & Technologies P.C. (FeedIT) is a spin off company of the Agricultural University of Athens that was founded in April 2024."
[[scope]]
  text = "► Feed Innovations & Technologies P.C. (FeedIT) is a spin off company of the Agricultural University of Athens that was founded in April 2024. "
[[scope]]
  text = "► The aim of FeedIT  is to exploit the long term know how of the team to design innovative nutritional products and technological tools and support services that will provide natural, economic, efficient and sustainable solutions in the Animal Production industry and the agri-food sector."
[[members]]
  name = "K.C. Mountzouris"
  role = "Professor"
  specialization = "Managing Director responsible for FeedIT nutritional products, communications, finance and growth development."
  image = "../img/mountzouris_constantinos.jpg"
  facebook = "https://facebook.com"
  googleplus = "https://google.com"
  xtwitter = "https://x.com"
  linkedin = "https://linkedin.com"
  mail = "test@gmail.com"
[[members]]
  name = "Dr. V.V. Paraskeuas"
  role = "Assistant Professor"
  specialization = "Responsible for experimentation, research programs management and administration."
  image = "../img/vparaskeuas.jpg"
[[members]]
  name = "Dr. I.N. Palamidi"
  role = "Postdoctoral researcher"
  specialization = "Responsible for research and development activities, focus on microbials."
  image = "../img/palamidi.png"
[[members]]
  name = "Mr I.P. Brouklogiannis"
  role = "PhD student"
  specialization = "Responsible for research and development activities, focus on analytical technological tools."
  image = "../img/brouk.jpg"
[[causes]]
 title = "Who We Are"
 text = "Feed Innovations & Technologies P.C. (FeedIT) is a spin off company of the Agricultural University of Athens that was founded in April 2024."
 background = "#f8c023"
 form = "circle"
[[causes]]
 title = "Our Aim"
 text = "The aim of FeedIT  to exploit the long term know how of Prof. KC Mountzouris and his team to design innovative nutritional products and technological tools and support services that will provide natural, economic, efficient and sustainable solutions in the Animal Production industry and the agri-food sector.​"
 background = "#f8c023"
 form = "circle"
[[causes]]
 title = "Subcontracting"
 text = "The expertise of the group of shareholders will be augmented on a subcontracting basis by expert scientists (e.g. chemical engineer, chemist, food technologist, biotechnologist, environmentalist, etc.)​"
 background = "#f8c023"
 form = "circle"
[[causes]]
 title = "Outsourcing"
 text = "FeedIT has a dedicated outsourced accounting and legal advice office.                                       ​"
 background = "#f8c023"
 form = "circle"
+++
# About Us​

Feed Innovations & Technologies P.C. (FeedIT) is a spin off company of the Agricultural University of Athens that was founded in April 2024. ​

The aim of FeedIT  to exploit the long term know how of Prof. KC Mountzouris and his team to design innovative nutritional products and technological tools and support services that will provide natural, economic, efficient and sustainable solutions in the Animal Production industry and the agri-food sector.​
​
Other team members include excellent Chemists, Chemical Engineers, Biotechnologists, Economists and Veterinarians that support FeedIT processes on demand.